# 🧠 Coding Challenge

Welcome to your coding challenge! We hope we have found an interesting task for you and looking forward to discuss your solution. If any questions pop up, please open an issue in this repository.

## Description

You will be creating a mobile optimised React application which is like "Tinder for Movies". The application chooses randomly assets from the collection of all popular movies and shows its image cover and title. The user can decide whether they like it or not by pressing on the correct button. If they like it they press the Love (Like) button, if they do not, they press the Poo (Dislike) button. Additionally the user is also able to use swipe the images to the right if they like it and to the left if they do not like it. All loved/liked assets will be stored in the user profile. As soon it was saved successfully, the next asset cover will appear.

![Wireframe](./Wireframe.png)

You can find the Sketch file and thus all the measurements under `./designs/tinder-for-movies.sketch`.

The data will be available over The Movie Database and the favourite movie will be stored with the connected user account. All API endpoints are available in the [documentation](https://developers.themoviedb.org/3/getting-started/introduction)

## Technical Details

**Fetch movies from TMDB**

You will need to fetch the data of all popular movies from The Movie Database (TMDB) in order to receive images and title. In order to work with it you need to create an account [here](https://www.themoviedb.org/signup) and create the API Key in your [user settings](https://www.themoviedb.org/settings/api).

Here you will find further links to receive data and images.

| Description           | Value                                                      | Links                                                         |
| --------------------- | ---------------------------------------------------------- | ------------------------------------------------------------- |
| Get Popular Movies    | https://api.themoviedb.org/3/movie/popular                 | https://developers.themoviedb.org/3/movies/get-popular-movies |
| Get Movie             | https://api.themoviedb.org/3/movie/{movie_id}              | https://developers.themoviedb.org/3/movies/get-movie-details  |
| Get Images            | https://image.tmdb.org/t/p/{image-size}/{image-path}       | https://developers.themoviedb.org/3/getting-started/images    |
| Mark as Favorite      | https://api.themoviedb.org/3/account/{account_id}/favorite | https://developers.themoviedb.org/3/account/mark-as-favorite  |
| MovieDB Documentation | https://developers.themoviedb.org/3/getting-started        |                                                               |

## Notes

- Please keep an eye on performance, rendering and possible side effects when handling requests
- Unit tests can be helpful and sometimes not needed
- No need to support all browsers
- Use any CSS library of your choice

❔ Please ask any question if something is not understandable. Just open a new issue and state your question.

👾 Happy coding and may the force be with you!
