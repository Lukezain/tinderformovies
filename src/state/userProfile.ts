import { atom } from "recoil";
import { recoilPersist } from 'recoil-persist'
import { UserProfile } from "../models/UserProfile";
const { persistAtom } = recoilPersist({
    key: 'demo', // this key is using to store data in local storage
    storage: localStorage, // configurate which storage will be used to store the data
  })

  export const userProfileState = atom<UserProfile>({
    key: "userProfileState", // unique ID (with respect to other atoms/selectors)
    default: {movies: []}, // default value (aka initial value)
    effects_UNSTABLE: [persistAtom],
  });
