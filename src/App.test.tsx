import { render } from '@testing-library/react';
import { RecoilRoot } from 'recoil';
import nock from 'nock';
import App from './App';
import Movie from './components/Movie';

test('app renders without crashing', () => {
  const { baseElement } = render(<RecoilRoot><App /></RecoilRoot>);
  expect(baseElement).toBeDefined();
});

test('retrieve movies', () => {
  const scope = nock('https://api.themoviedb.org')
    .get(`/3/movie/popular?api_key=${process.env.REACT_APP_API_KEY}`)
    .once()
    .reply(200, {
      data: 'response',
    });

    scope.isDone()
    expect(scope).toBeDefined()
})

test('Movie renders successfully without crashing when data is missing', () => {
  const { baseElement } = render(<Movie handlers={undefined} activeImg={''} movies={[]} handleMovieAction={undefined}/>);
  expect(baseElement).toBeDefined();
});