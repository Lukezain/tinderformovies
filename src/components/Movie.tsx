import { FaHeart, FaPoop } from "react-icons/fa";
import classes from "../index.module.css";
import { MovieModel } from "../models/Movie";

interface MovieProps {
  handlers: any;
  activeImg: string;
  movies: MovieModel[];
  handleMovieAction: any;
}

const Movie: React.FC<MovieProps> = ({
  handlers,
  activeImg,
  movies,
  handleMovieAction,
}) => {
  return (
    <section {...handlers} className={classes.movieContainer}>
      <div className={classes.poster}>
        <img src={`${activeImg}`} alt="Movie Poster" />
        <div className={classes.movieTitle}>{movies[0] && movies[0].title}</div>
      </div>
      <div className={classes.buttons}>
        <div
          onClick={() => handleMovieAction(false)}
          className={classes.button + " " + classes.pooButton}
        >
          <FaPoop />
        </div>
        <div
          onClick={() => handleMovieAction(true)}
          className={classes.button + " " + classes.loveButton}
        >
          <FaHeart />
        </div>
      </div>
    </section>
  );
};

export default Movie;
