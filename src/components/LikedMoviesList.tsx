import { FaHeart } from "react-icons/fa";
import classes from "../index.module.css";
import { UserProfile } from "../models/UserProfile";

interface likedMovieListProps {
    userProfile:UserProfile
}

const LikedMoviesList: React.FC<likedMovieListProps> = ({userProfile}) => {
    return (
        <section className={classes.movieList}>
            <h2>Liked Movies</h2>
            <ul>
              {userProfile.movies.length > 0 ? (
                userProfile.movies.map((movie) => {
                  if (movie.liked) {
                    return (
                      <li key={movie.id}>
                        <FaHeart /> <div>{movie.title}</div>
                      </li>
                    );
                  } else {
                      return null
                  }
                })
              ) : (
                <li>Nothing yet, go like some movies!</li>
              )}
            </ul>
          </section>
    )
}

export default LikedMoviesList
