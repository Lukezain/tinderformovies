import { useEffect, useState } from "react";
import classes from "./index.module.css";
import axios from "axios";
import { MovieModel } from "./models/Movie";
import { ErrorModel } from "./models/Error";
import { useSwipeable } from "react-swipeable";
import { useRecoilState } from "recoil";
import { userProfileState } from "./state/userProfile";
import shuffle from "lodash/shuffle";
import LikedMoviesList from "./components/LikedMoviesList";
import Movie from "./components/Movie";

function App() {
  const [loading, setLoading] = useState(false);
  const [showList, setShowList] = useState(false);

  const [userProfile, setUserProfile] = useRecoilState(userProfileState);

  const [pageNumber, setPageNumber] = useState(1);
  const [movies, setMovies] = useState<MovieModel[]>();
  const [activeImg, setActiveImg] = useState("");

  const [error, setError] = useState<ErrorModel | null>(null);

  useEffect(() => {
    //runs on first load and whenever pageNumber's value is changed
    getMovies();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageNumber]);

  /**
   * calls the moviedb api to get a list of popular movies
   * then filteres the movies using the filterMovies function to return
   * only the movies that haven't been liked/disliked yet
   * if filtered movies length is zero (meaning all the movies have already been interacted with)
   * it will update the page number which will run the effect to call getMovies with the new page number
   * if any error was found, it will set the error state accordingly
   */
  const getMovies = () => {
    setLoading(true);
    axios
      .get(
        `https://api.themoviedb.org/3/movie/popular?api_key=${process.env.REACT_APP_API_KEY}&page=${pageNumber}`
      )
      .then((res) => {
        let movies = res.data.results;
        filterMovies(movies).then((filtered: any) => {
          if (filtered.length === 0) {
            setPageNumber(pageNumber + 1);
          } else {
            getNextMovie(shuffle(filtered), false);
            setError(null);
          }
          setLoading(false);
        });
      })
      .catch((err) => {
        if (err.response) {
          setError(err.response.data);
        }
        setLoading(false);
      });
  };

  /**
   * prepares the next movie to be shown, by shuffling the movies array and checking if it is needed to advance to the next page.
   *
   * @param {MovieModel[]} movies The movies array.
   * @param {boolean} splice clause weather to remove the first element from the movie array.
   */
  const getNextMovie = (movies: MovieModel[], splice: boolean) => {
    let moviesNew = [...movies];
    if (splice) moviesNew.splice(0, 1);
    if (moviesNew.length === 0) {
      // get next page
      setPageNumber(pageNumber + 1);
      return;
    } else {
      // using _shuffle function to randomly shuffly the order of the movies array
      let shuffled = shuffle(moviesNew);
      setLoading(true);
      // loading the image before continuing to avoid showing a blank screen on slow connections
      axios
        .get(`https://image.tmdb.org/t/p/w500${shuffled[0].poster_path}`, {
          responseType: "blob",
        })
        .then((response) => {
          // converting the img blob to base64 format to be able to dynamically change the movie poster
          const fileReaderInstance = new FileReader();
          fileReaderInstance.readAsDataURL(response.data);
          fileReaderInstance.onload = () => {
            let base64data = fileReaderInstance.result;
            if (base64data) {
              setActiveImg(base64data.toString());
              setMovies(shuffled);
              setLoading(false);
            } else {
              setError({status_code:500, success:false, status_message:"There was a problem retrieving the movie poster"})
            }
          };
        })
        .catch((error) => {
          console.log(error.response);
        });
    }
  };

  //swipe gesture recognition event handlers
  const handlers = useSwipeable({
    onSwipedLeft: () => {
      //user swiped left and disliked
      handleMovieAction(false);
    },
    onSwipedRight: () => {
      //user swiped right and liked
      handleMovieAction(true);
    },
    trackMouse: true
  });

  /**
   * function to handle the swipe or button press to add the movie to the list of movies in the userProfile
   * and then calls getNextMovie with splice clause set to true
   * @param {boolean} val liked = true, disliked = false.
   */
  const handleMovieAction = (val: boolean) => {
    if (movies && !showList) {
      setUserProfile({
        movies: [
          ...userProfile.movies,
          {
            id: movies[0].id,
            title: movies[0].title,
            liked: val,
          },
        ],
      });
      getNextMovie(movies, true);
    }
  };

  /**
   * filters the movies array by removing movies that were already added to the userProfile list.
   *
   * @param {MovieModel[]} movies The array of movies.
   * @return {MovieModel[]} the filtered array of movies.
   */
  const filterMovies = (movies: MovieModel[]) => {
    return new Promise((resolve, reject) => {
      let filtered = movies.filter((movie) => {
        return !checkIfExists(movie);
      });
      resolve(filtered);
    });
  };

  /**
   * checks if the given movie exists in the list of movies in the userProfile.movies array.
   *
   * @param {MovieModel} movie the movie to check.
   * @return {boolean} if the movie exists in userProfile.movies or not.
   */
  const checkIfExists = (movie: MovieModel) => {
    let exists = false;
    for (let likedMovie of userProfile.movies) {
      if (likedMovie.id === movie.id) {
        exists = true;
      }
    }
    return exists;
  };

  return (
    <div className="App">
      <header>
        <div onClick={() => setShowList(!showList)} className={classes.logo}>
          Movies
        </div>
      </header>
      <main className={classes.main}>
        {loading && (
          <div className={classes.loaderContainer}>
            <div className={classes.loader}></div>
          </div>
        )}
        {showList ? (
          <LikedMoviesList userProfile={userProfile} />
        ) : movies ? (
          <Movie
            handlers={handlers}
            activeImg={activeImg}
            handleMovieAction={handleMovieAction}
            movies={movies}
          />
        ) : error ? (
          <section className={classes.errorSection}>
            <p>There was a problem obtaining the list of movies</p>
            <p>Message: {error.status_message}</p>
          </section>
        ) : (
          <section className={classes.errorSection}>
            <p>There are no movies to show</p>
          </section>
        )}
      </main>
    </div>
  );
}

export default App;
