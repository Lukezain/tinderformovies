interface userMovie {
    id: number;
    title: string;
    liked: boolean;
}

export interface UserProfile {
    movies: userMovie[]
}