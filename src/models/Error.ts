export interface ErrorModel {
    status_code: number;
    status_message: string;
    success: boolean;
  }
  